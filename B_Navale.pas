program B_Navale;

uses crt;

type 
	TabDeChar=array [0..11,0..11] of string;
	
	coord=RECORD
		x1:integer;
		y1:integer;
		x2:integer;
		y2:integer;
	end;
	
	cara=RECORD
		l:integer;
		lim:integer;
		pos:coord;
		nom:string;
		
	end;
	
	bt=RECORD
		PorteAvion:cara;
		Cuirasse:cara;
		Destroyer:cara;
		SousMarin:cara;
		Torpilleur:cara;
	end;
	
	j = RECORD
			j1:bt;
			j2:bt;
	end;
	
	map = RECORD
		tabjoueur1:TabDeChar;
		tabjoueur2:TabDeChar;
		tabattaque1:TabDeChar;
		tabattaque2:TabDeChar;
	end;
	
	plateau = RECORD
		carte:map;
		joueur:j;
	end;

var
don:string;
pa,cr,dt,ss,tp:j;
plat:plateau;

Procedure Init();

	begin
		plat.joueur.j1.PorteAvion.l:=5;
		plat.joueur.j2.PorteAvion.l:=5;
		plat.joueur.j1.Cuirasse.l:=4;
		plat.joueur.j2.Cuirasse.l:=4;
		plat.joueur.j1.Destroyer.l:=3;
		plat.joueur.j2.Destroyer.l:=3;
		plat.joueur.j1.SousMarin.l:=3;
		plat.joueur.j2.SousMarin.l:=3;
		plat.joueur.j1.Torpilleur.l:=2;
		plat.joueur.j2.Torpilleur.l:=2;

		plat.joueur.j1.PorteAvion.lim:=7;
		plat.joueur.j2.PorteAvion.lim:=7;
		plat.joueur.j1.Cuirasse.lim:=8;
		plat.joueur.j2.Cuirasse.lim:=8;
		plat.joueur.j1.Destroyer.lim:=9;
		plat.joueur.j2.Destroyer.lim:=9;
		plat.joueur.j1.SousMarin.lim:=9;
		plat.joueur.j2.SousMarin.lim:=9;
		plat.joueur.j1.Torpilleur.lim:=10;
		plat.joueur.j2.Torpilleur.lim:=10;

		plat.joueur.j1.PorteAvion.nom:='porte avion';
		plat.joueur.j2.PorteAvion.nom:='porte avion';
		plat.joueur.j1.Cuirasse.nom:='cuirasse';
		plat.joueur.j2.Cuirasse.nom:='cuirasse';
		plat.joueur.j1.Destroyer.nom:='destroyer';
		plat.joueur.j2.Destroyer.nom:='destroyer';
		plat.joueur.j1.SousMarin.nom:='sous-marin';
		plat.joueur.j2.SousMarin.nom:='sous-marin';
		plat.joueur.j1.Torpilleur.nom:='torpilleur';
		plat.joueur.j2.Torpilleur.nom:='torpilleur';
	end;

Procedure Reset();
	var i,j:integer;

	begin
		For i:=0 to 10 do
		Begin
			for j:=0 to 10 do
			begin
				plat.carte.tabjoueur1[i,j]:='_ ';
				plat.carte.tabjoueur2[i,j]:='_ ';
				plat.carte.tabattaque1[i,j]:='_ ';
				plat.carte.tabattaque2[i,j]:='_ ';
			end;
		end;
	end;

Procedure Affichage(tabjoueur:TabDeChar;don:string);
	var i,j:integer;

	begin
		clrscr;
		For i:=0 to 10 do
		begin
			tabjoueur[i,0]:=don[i+1]+' ';
			tabjoueur[0,i]:=don[i+1]+' ';
		end;
		tabjoueur[10,0]:='10';
		tabjoueur[0,10]:='10';
		For i:=0 to 10 do
		begin
			for j:=0 to 10 do
			begin
				if tabjoueur[j,i]='_ ' then
				begin
					TextColor(Blue);
					write(tabjoueur[j,i]);
					TextColor(White);
				end
				else
				begin
					if tabjoueur[j,i]='* ' then
					begin
						TextColor(Yellow);
						write(tabjoueur[j,i]);
						TextColor(White);
					end
					else
					begin
						if tabjoueur[j,i]='B ' then
						begin
							TextColor(Green);
							write(tabjoueur[j,i]);
							TextColor(White);
						end
						else
						begin
							if tabjoueur[j,i]='X ' then
							begin
								TextColor(Red);
								write(tabjoueur[j,i]);
								TextColor(White);
							end
							else
							begin
								if tabjoueur[j,i]='O ' then
								begin
									TextColor(Blue);
									write(tabjoueur[j,i]);
									TextColor(White);
								end
								else
								begin
									write(tabjoueur[j,i]);
								end;
							end;
						end;
					end;
				end;
			end;
			writeln;
		end;
	end;

Function Sens(ok:boolean;ship:string;joueuractif:integer):char;
	var p:char;

	begin
		while ok=false do
		begin
			writeln ('Joueur ',joueuractif,', voulez vous inserer votre ',ship,' horizontalement (H) ou verticalement (V)?');
			readln(p);
			if (p='H') or (p='V') then
			begin
				ok:=true;
			end;
		end;
		Sens:=p;
	end;

Function InsereBateau(tab:TabDeChar;p:char;lim,l:integer):TabDeChar;

	var ok,placement:boolean;
		x,y,i,j:integer;

	begin
		ok:=false;
		placement:=true;
		while ok=false do
		begin
			writeln('Veuillez entrer les coordonnees de la pointe du navire');
			writeln('X : ');
			readln(x);
			writeln('Y : ');
			readln(y);
			if (x<11) and (y<11) then //Check si la pointe du navire se trouve dans la carte
			begin
				if (p='H') and (x<lim) then //Check si le bateau tient dans la carte
				begin
					for i:=1 to l do //Check si un bateau est sur le chemin
					begin
						if (tab[x+i-1,y]='B ') or (tab[x+1-1,y]='* ') then
						begin
							placement:=false;
						end;
					end;
					if placement=true then //Si pas de bateau dans le chemin, création d'un bateau
					begin
						TextColor(Red);
						if tab[x-1,y]='_ ' then
							tab[x-1,y]:='* ';
						if tab[x-1,y-1]='_ ' then
							tab[x-1,y-1]:='* ';
						if tab[x-1,y+1]='_ ' then
							tab[x-1,y+1]:='* ';
						TextColor(White);
						for i:=1 to l do
						begin
							tab[x+i-1,y-1]:='* ';
							tab[x+i-1,y]:='B ';
							tab[x+i-1,y+1]:='* ';
						end; 
						if tab[x+i,y]='_ ' then
							tab[x+i,y]:='* ';
						if tab[x+i,y+1]='_ ' then
							tab[x+i,y+1]:='* ';
						if tab[x+i,y-1]='_ ' then
						tab[x+i,y-1]:='* ';
						ok:=true;
					end;
				end;
				if (p='V') and (y<lim) then
				begin
					for i:=1 to l do //Check si un bateau est sur le chemin
					begin
						if (tab[x,y+i-1]='B ') or (tab[x,y+i-1]='* ') then
						begin
							placement:=false;
						end;
					end;
					if placement=true then //Si pas de bateau dans le chemin, création d'un bateau
					begin
						if tab[x,y-1]='_ ' then
							tab[x,y-1]:='* ';
						if tab[x+1,y-1]='_ ' then
							tab[x+1,y-1]:='* ';
						if tab[x-1,y-1]='_ ' then
							tab[x-1,y-1]:='* ';
						for i:=1 to l do
						begin
							tab[x-1,y+i-1]:='* ';
							tab[x,y+i-1]:='B ';
							tab[x+1,y+i-1]:='* ';
						end; 
						if tab[x,y+i]='_ ' then
							tab[x,y+i]:='* ';
						if tab[x+1,y+i]='_ ' then
							tab[x+1,y+i]:='* ';
						if tab[x-1,y+i]='_ ' then
							tab[x-1,y+i]:='* ';
						ok:=true;
					end;
				end;
			end;
			placement:=true; 
		end;
		InsereBateau:=tab;
	end;

Procedure Tour_Pose(don:string);
	
	var joueuractif,lim,l:integer;
		ok,placement:boolean;

	begin
		joueuractif:=1;
		plat.carte.tabjoueur1:=InsereBateau(plat.carte.tabjoueur1,Sens(ok,plat.joueur.j1.PorteAvion.nom,joueuractif),plat.joueur.j1.PorteAvion.lim,plat.joueur.j1.PorteAvion.l);
		Affichage(plat.carte.tabjoueur1,don);
		plat.carte.tabjoueur1:=InsereBateau(plat.carte.tabjoueur1,Sens(ok,plat.joueur.j1.Cuirasse.nom,joueuractif),plat.joueur.j1.Cuirasse.lim,plat.joueur.j1.Cuirasse.l);
		Affichage(plat.carte.tabjoueur1,don);
		plat.carte.tabjoueur1:=InsereBateau(plat.carte.tabjoueur1,Sens(ok,plat.joueur.j1.Destroyer.nom,joueuractif),plat.joueur.j1.Destroyer.lim,plat.joueur.j1.Destroyer.l);
		Affichage(plat.carte.tabjoueur1,don);
		plat.carte.tabjoueur1:=InsereBateau(plat.carte.tabjoueur1,Sens(ok,plat.joueur.j1.SousMarin.nom,joueuractif),plat.joueur.j1.SousMarin.lim,plat.joueur.j1.SousMarin.l);
		Affichage(plat.carte.tabjoueur1,don);
		plat.carte.tabjoueur1:=InsereBateau(plat.carte.tabjoueur1,Sens(ok,plat.joueur.j1.Torpilleur.nom,joueuractif),plat.joueur.j1.Torpilleur.lim,plat.joueur.j1.Torpilleur.l);
		Affichage(plat.carte.tabjoueur1,don);

		joueuractif:=2;
		Affichage(plat.carte.tabjoueur2,don);
		plat.carte.tabjoueur2:=InsereBateau(plat.carte.tabjoueur2,Sens(ok,plat.joueur.j2.PorteAvion.nom,joueuractif),plat.joueur.j2.PorteAvion.lim,plat.joueur.j2.PorteAvion.l);
		Affichage(plat.carte.tabjoueur2,don);
		plat.carte.tabjoueur2:=InsereBateau(plat.carte.tabjoueur2,Sens(ok,plat.joueur.j2.Cuirasse.nom,joueuractif),plat.joueur.j2.Cuirasse.lim,plat.joueur.j2.Cuirasse.l);
		Affichage(plat.carte.tabjoueur2,don);
		plat.carte.tabjoueur2:=InsereBateau(plat.carte.tabjoueur2,Sens(ok,plat.joueur.j2.Destroyer.nom,joueuractif),plat.joueur.j2.Destroyer.lim,plat.joueur.j2.Destroyer.l);
		Affichage(plat.carte.tabjoueur2,don);
		plat.carte.tabjoueur2:=InsereBateau(plat.carte.tabjoueur2,Sens(ok,plat.joueur.j2.SousMarin.nom,joueuractif),plat.joueur.j2.SousMarin.lim,plat.joueur.j2.SousMarin.l);
		Affichage(plat.carte.tabjoueur2,don);
		plat.carte.tabjoueur2:=InsereBateau(plat.carte.tabjoueur2,Sens(ok,plat.joueur.j2.Torpilleur.nom,joueuractif),plat.joueur.j2.Torpilleur.lim,plat.joueur.j2.Torpilleur.l);
		Affichage(plat.carte.tabjoueur2,don);
	end;

Function Attaque(tabjoueur,tabattaque:TabDeChar;don:string;joueur:integer):TabDeChar;

	var x,y:integer;
		ok:boolean;

	Begin
		ok:=false;
		Affichage(tabattaque,don);
		writeln('Veuillez entrer les coordonnees de tir joueur',joueur);
		while ok=false do
		begin
			writeln('X : ');
			readln(x);
			writeln('Y : ');
			readln(y);
			if (x>0) and (x<11) and (y>0) and (y<11) then
			begin
				if tabattaque[x,y]='_ ' then
				begin
					ok:=true;
					if tabjoueur[x,y]='B ' then
					begin
						tabattaque[x,y]:='X ';
						Affichage(tabattaque,don);
						writeln ('Toucher');
					end
					else
					begin
						tabattaque[x,y]:='O ';
						Affichage(tabattaque,don);
						writeln ('A l''eau');
					end;
				end;
			end;
		end;
		Attaque:=tabattaque;
	End;

Function CheckVie(tab:TabDeChar):boolean;

	var mort:boolean;
		nbX,i,j:integer;

	Begin
		nbX:=0;
		mort:=false;
		for i:=1 to 10 do
		begin
			for j:=1 to 10 do
			begin
				if tab[i,j]='X ' then
					nbX:=nbX+1;
			end;
		end;
		if nbX=17 then
			mort:=true;
		CheckVie:=mort
	End;

Procedure Jeu();
	var fin:boolean;
		joueuractif:integer;

	Begin
		fin:=false;
		while fin=false do
		begin
			if (fin=false) then
			begin
				joueuractif:=1;
				plat.carte.tabattaque1:=Attaque(plat.carte.tabjoueur2,plat.carte.tabattaque1,don,joueuractif);
				fin:=CheckVie(plat.carte.tabattaque1);
				readln;
			end;
			if (fin=false) then
			begin
				joueuractif:=2;
				plat.carte.tabattaque2:=Attaque(plat.carte.tabjoueur1,plat.carte.tabattaque2,don,joueuractif);
				fin:=CheckVie(plat.carte.tabattaque2);
				readln;
			end;
		end;
		if joueuractif=1 then
		begin
			writeln('Joueur1 gagne la partie!');
		end;
		if joueuractif=2 then
		begin
			writeln('Joueur2 gagne la partie!');
		end;
		readln;
	End;

BEGIN
	don:=' 1234567890';
	Reset();
	Init();
	Affichage(plat.carte.tabjoueur1,don);
	Tour_Pose(don);
	Jeu();
	readln;
END.

